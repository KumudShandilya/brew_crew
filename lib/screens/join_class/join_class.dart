import 'package:brew_crew/models/user.dart';
import 'package:brew_crew/screens/home/home.dart';
import 'package:brew_crew/services/database.dart';
import 'package:brew_crew/shared/constants.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class JoinClass extends StatefulWidget {
  @override
  _JoinClassState createState() => _JoinClassState();
}

class _JoinClassState extends State<JoinClass> {
  final _formKey = GlobalKey<FormState>();

  void goBackToHomePage() {
    Route route = MaterialPageRoute(builder: (context) => Home());
    Navigator.push(context, route);
  }

  // text field state
  String classCode = '';

  //String teacherCode = '';

  Widget build(BuildContext context) {
    final user = Provider.of<UserClass>(context);
    Size size = MediaQuery.of(context).size;
    String uid = user.uid;
    return Scaffold(
      appBar: AppBar(
        title: Text('Virtual Kaksha'),
        backgroundColor: Color(0xFF6F35A5),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              Text("JOIN CLASS",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
              SizedBox(height: size.height * 0.03),
              Image.asset(
                "assets/images/class.jpg",
                height: size.height * 0.35,
              ),
              SizedBox(height: 20.0),
              TextFormField(
                decoration:
                    textInputDecoration.copyWith(hintText: 'Class Code'),
                validator: (val) =>
                    val.isEmpty ? 'Enter a valid class Code' : null,
                onChanged: (val) {
                  setState(() => classCode = val);
                },
              ),
              SizedBox(height: 20.0),
              MaterialButton(
                  color: Color(0xFF6F35A5),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)
                  ),
                  child: Text(
                    'Join',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () async {
                    if (_formKey.currentState.validate()) {
                      print(uid);
                      await DatabaseService(uid: uid)
                          .joinClassUsingCode(classCode);
                      setState(() => classCode = "");
                      //setState(() => teacherCode = "");
                      goBackToHomePage();
                    }
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
