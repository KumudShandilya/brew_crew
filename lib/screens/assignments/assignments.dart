import 'package:brew_crew/models/user.dart';
import 'package:brew_crew/screens/assignment_page/assignment_page.dart';
import 'package:brew_crew/screens/create_assignment/create_assignment.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Assignments extends StatefulWidget {
  final String name;
  final String description;
  final String owner;
  final String id;

  const Assignments({Key key, this.name, this.description, this.owner, this.id})
      : super(key: key);

  @override
  _AssignmentsState createState() => _AssignmentsState();
}

class _AssignmentsState extends State<Assignments> {
  onGoBack(dynamic value) {
    setState(() {});
  }

  void navigateToMakeAnnouncement() {
    Route route = MaterialPageRoute(
        builder: (context) => CreateAssignment(class_id: widget.id));
    Navigator.push(context, route).then(onGoBack);
  }

  Widget build(BuildContext context) {
    final user = Provider.of<UserClass>(context);

    String uid = user.uid;

    bool is_owner = false;

    if (widget.owner == uid) is_owner = true;

    CollectionReference assignmentsForThisClass = FirebaseFirestore.instance
        .collection('assignments')
        .doc(widget.id)
        .collection('_assignments');

    return Scaffold(
      appBar: AppBar(
        title: Text('Assignments'),
        backgroundColor: Color(0xFF6F35A5),
        actions: <Widget>[
          /*Visibility(
              visible: is_owner,
              child: OutlinedButton.icon(
                icon: Icon(Icons.edit_location),
                label: Text('Create Assignment'),
                onPressed: () {
                  navigateToMakeAnnouncement();
                },
              )),*/
        ],
      ),
      floatingActionButton: Visibility(
          visible: is_owner,
          child: FloatingActionButton(
            onPressed: () {
              navigateToMakeAnnouncement();
            },
            child: Icon(Icons.add),
            backgroundColor: Color(0xFF6F35A5),
          )),
      body: FutureBuilder<QuerySnapshot>(
        future: assignmentsForThisClass.get(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            print("Has data");
            print(snapshot.data);

            print(snapshot.data.docs.length);

            //	print(snapshot.data.docs[0]);

          } else
            print("no assignment");
          // print(snapshot.hasError);
          if (snapshot.hasError) {
            return Text('Something went wrong');
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text("Loading");
          }

          if (snapshot.data.docs.length == 0) {
            return Text("No assignments");
          }

          return ListView(
            children: snapshot.data.docs.map((DocumentSnapshot document) {
              Map<String, dynamic> data =
                  document.data() as Map<String, dynamic>;

              // _Class c = _Class(data['name'], data['description'], data['owner'], data['id']);
              return GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => AssignmentPage(
                              class_id: widget.id,
                              assign_id: data['id'],
                              name: data['assign_name'],
                              description: data['assign_description'],
                              urlDown: data['urlDownload'],
                              owner: widget.owner))).then(onGoBack);
                },
                child: Container(
                  child: Row(
                    children: <Widget>[
                      Container(
                        height: 50,
                        decoration: BoxDecoration(),
                        child: Image.asset('assets/images/ann.jpg'),
                      ),
                      SizedBox(
                        width: 40.0,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          if (data['urlDownload'] != null) ...[
                            Text(data['assign_name']),
                            Text(data['assign_description']),
                            Text(data['urlDownload']),
                          ] else ...[
                            Text(data['assign_name']),
                            Text(data['assign_description']),
                            //  Text(data['urlDownload']),
                          ]
                        ],
                      ),
                    ],
                  ),
                  width: 10,
                  margin: EdgeInsets.only(
                      left: 10.0, top: 10.0, right: 10.0, bottom: 10.0),
                  decoration: BoxDecoration(
                    color: Color(0xFFF1E6FF),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(5),
                        topRight: Radius.circular(5),
                        bottomLeft: Radius.circular(5),
                        bottomRight: Radius.circular(5)),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 1,
                        blurRadius: 2,
                        offset: Offset(0, 1), // changes position of shadow
                      ),
                    ],
                  ),
                ),
              );
            }).toList(),
          );
        },
      ),
    );
  }
}
