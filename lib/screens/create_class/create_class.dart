import 'package:brew_crew/models/user.dart';
import 'package:brew_crew/screens/home/home.dart';
import 'package:brew_crew/services/database.dart';
import 'package:brew_crew/shared/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

class CreateClass extends StatefulWidget {
  @override
  _CreateClassState createState() => _CreateClassState();
}

class _CreateClassState extends State<CreateClass> {
  final _formKey = GlobalKey<FormState>();

  void goBackToHomePage() {
    Route route = MaterialPageRoute(builder: (context) => Home());
    Navigator.push(context, route);
  }

  // text field state
  String name = '';
  String description = '';

  Widget build(BuildContext context) {
    var uuid = Uuid();
    Size size = MediaQuery.of(context).size;
    final user = Provider.of<UserClass>(context);
    String uid = user.uid;

    return Scaffold(
      appBar: AppBar(
				title: Text('Virtual Kaksha'),
				backgroundColor: Color(0xFF6F35A5),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
							Text("CREATE CLASS",
									style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
							SizedBox(height: size.height * 0.03),
							Image.asset(
								"assets/images/class.jpg",
								height: size.height * 0.35,
							),
              SizedBox(height: 20.0),
              TextFormField(
                decoration:
                    textInputDecoration.copyWith(hintText: 'Class Name'),
                validator: (val) =>
                    val.isEmpty ? 'Enter an name for class' : null,
                onChanged: (val) {
                  setState(() => name = val);
                },
              ),
              SizedBox(height: size.height * 0.03),
              TextFormField(
                decoration:
                    textInputDecoration.copyWith(hintText: 'Class Description'),
                validator: (val) => val.isEmpty ? 'Enter description' : null,
                onChanged: (val) {
                  setState(() => description = val);
                },
              ),
              SizedBox(height: 20.0),
              MaterialButton(
                  color: Color(0xFF6F35A5),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)
                  ),
                  child: Text(
                    'Create',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () async {
                    if (_formKey.currentState.validate()) {
                      print(uid);
                      await DatabaseService(uid: uid)
                          .createNewClass(name, description, uuid.v4());
                      setState(() => description = "");
                      setState(() => name = "");
                      goBackToHomePage();
                    }
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
