import 'package:brew_crew/models/user.dart';
import 'package:brew_crew/screens/assignments/assignments.dart';
import 'package:brew_crew/screens/create_announcement/create_announcement.dart';
import 'package:brew_crew/screens/students/students.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class Announcements extends StatefulWidget {
  final String name;
  final String description;
  final String owner;
  final String id;

  const Announcements(
      {Key key, this.name, this.description, this.owner, this.id})
      : super(key: key);

  @override
  _AnnouncementsState createState() => _AnnouncementsState();
}

class _AnnouncementsState extends State<Announcements> {
  onGoBack(dynamic value) {
    setState(() {});
  }

  void navigateToMakeAnnouncement() {
    Route route = MaterialPageRoute(
        builder: (context) => CreateAnnouncement(class_id: widget.id));
    Navigator.push(context, route).then(onGoBack);
  }

  void navigateToStudents() {
    Route route =
        MaterialPageRoute(builder: (context) => Students(class_id: widget.id));
    Navigator.push(context, route).then(onGoBack);
  }

  void navigateToAssignments() {
    Route route = MaterialPageRoute(
        builder: (context) => Assignments(
            name: widget.name,
            description: widget.description,
            owner: widget.owner,
            id: widget.id));
    Navigator.push(context, route).then(onGoBack);
  }

  Widget build(BuildContext context) {
    final user = Provider.of<UserClass>(context);

    String uid = user.uid;

    bool is_owner = false;

    if (widget.owner == uid) is_owner = true;

    CollectionReference announcementsForThisClass = FirebaseFirestore.instance
        .collection('announcements')
        .doc(widget.id)
        .collection('_announcements');

    return Scaffold(
      appBar: AppBar(
        title: Text('Announcements'),
        backgroundColor: Color(0xFF6F35A5),
        actions: <Widget>[
          OutlinedButton.icon(
            icon: Icon(Icons.person, color: Colors.white),
            label: Text('Students', style: TextStyle(color: Colors.white)),
            onPressed: () {
              navigateToStudents();
            },
          ),
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: Stack(
        fit: StackFit.expand,
        children: [
          Positioned(
            right: 20,
            bottom: 10,
            child: FloatingActionButton.extended(
              icon: Icon(Icons.assignment,
                  color: Colors.white,
                  size: 40),
              onPressed: () {navigateToAssignments();},
              label: Text('   Assignment  '),
              backgroundColor: Color(0xFF6F35A5),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
          if(is_owner)
          Positioned(
            bottom: 65,
            right: 20,
            child: FloatingActionButton.extended(
              icon: Icon(Icons.add,
                  color: Colors.white,
                  size: 40),
              onPressed: () {navigateToMakeAnnouncement();},
              label: Text('Announcement'),
              backgroundColor: Color(0xFF6F35A5),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
          // Add more floating buttons if you want
          // There is no limit
        ],
      ),
      /*floatingActionButton: Visibility(
          visible: is_owner,
          child: FloatingActionButton(
            onPressed: () {
              navigateToMakeAnnouncement();
            },
            child: Icon(Icons.add),
          )),*/
      body: FutureBuilder<QuerySnapshot>(
        future: announcementsForThisClass.get(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            print("Has data");
            print(snapshot.data);

            print(snapshot.data.docs.length);

            //print(snapshot.data.docs[0]);

          } else
            print("no announcements");
          // print(snapshot.hasError);
          if (snapshot.hasError) {
            return Text('Something went wrong');
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text("Loading");
          }

          if (snapshot.data.docs.length == 0) {
            return Text("No announcements");
          }

          return ListView(
            children: snapshot.data.docs.map((DocumentSnapshot document) {
              Map<String, dynamic> data =
                  document.data() as Map<String, dynamic>;

              // _Class c = _Class(data['name'], data['description'], data['owner'], data['id']);
              return GestureDetector(
                child: Container(

                  child: Row(
                    children: <Widget>[
                      Container(
                        height: 50,
                        decoration: BoxDecoration(),
                        child: Image.asset('assets/images/ann.jpg'),
                      ),
                      SizedBox(
                        width: 40.0,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          if (data['urlDownload'] != null) ...[
                            Text(data['announce_name']),
                            Text(data['announce_description']),
                            GestureDetector(
                              onTap: () async {
                                var url = data['urlDownload'];
                                print(url);

                                if (await canLaunch(url)) {
                                  await launch(url);
                                } else
                                  print("could not launch");
                              },
                              child: Text("Download attachment"),
                            ),
                          ] else ...[
                            Text(data['announce_name']),
                            Text(data['announce_description']),
                            //  Text(data['urlDownload']),
                          ]
                        ],
                      ),
                    ],
                  ),
                  width: 10,
                  margin: EdgeInsets.only(
                      left: 10.0, top: 10.0, right: 10.0, bottom: 10.0),
                  decoration: BoxDecoration(
                    color: Color(0xFFF1E6FF),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(5),
                        topRight: Radius.circular(5),
                        bottomLeft: Radius.circular(5),
                        bottomRight: Radius.circular(5)),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 1,
                        blurRadius: 2,
                        offset: Offset(0, 1), // changes position of shadow
                      ),
                    ],
                  ),
                ),
              );
            }).toList(),
          );
        },
      ),
    );
  }
}
