import 'package:brew_crew/models/user.dart';
import 'package:brew_crew/screens/create_class/create_class.dart';
import 'package:brew_crew/screens/home/renderCreatedClasses.dart';
import 'package:brew_crew/screens/join_class/join_class.dart';
import 'package:brew_crew/services/auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final AuthService _auth = AuthService();

  onGoBack(dynamic value) {
    setState(() {});
  }

  void navigateToCreateClassForm() {
    Route route = MaterialPageRoute(builder: (context) => CreateClass());
    Navigator.push(context, route).then(onGoBack);
  }

  void navigateToJoinClassForm() {
    Route route = MaterialPageRoute(builder: (context) => JoinClass());
    Navigator.push(context, route).then(onGoBack);
  }

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserClass>(context);

    String uid = user.uid;
    String user_name = user.user_name;
    print(user_name);

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text('Virtual Kaksha'),
        backgroundColor: Color(0xFF6F35A5),
        elevation: 0.0,
        actions: <Widget>[
          OutlinedButton.icon(
            icon: Icon(Icons.person, color: Colors.white),
            label: Text('Log out', style: TextStyle(color: Colors.white)),
            onPressed: () async {
              setState(() {});
              await _auth.signOut();
            },
          ),
        ],
      ),

      /* body:  FlatButton.icon(


        	icon: Icon(Icons.edit_location),
              label: Text(
                'Create Class'
              ),
              onPressed: () {
                Navigator.pushNamed(context, '/create_class');
              },

            ),
            */
      body: UserInformation(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: Stack(
        fit: StackFit.expand,
        children: [
          Positioned(
            left: 20,
            bottom: 10,
            child: FloatingActionButton.extended(
              icon: Icon(Icons.add_circle,
                color: Colors.white,
                size: 40),
              onPressed: () {navigateToCreateClassForm();},
              label: Text('Create Class'),
              backgroundColor: Color(0xFF6F35A5),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
          Positioned(
            bottom: 10,
            right: 20,
            child: FloatingActionButton.extended(
              icon: Icon(Icons.meeting_room,
                  color: Colors.white,
                  size: 40),
              onPressed: () {navigateToJoinClassForm();},
              label: Text('Join Class'),
              backgroundColor: Color(0xFF6F35A5),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
          // Add more floating buttons if you want
          // There is no limit
        ],
      ),
    );
  }
}
