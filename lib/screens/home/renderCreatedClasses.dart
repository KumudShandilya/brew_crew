import 'package:flutter/material.dart';

// Import the firebase_core and cloud_firestore plugin
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:brew_crew/services/database.dart';
import 'package:brew_crew/models/classes.dart';
import 'package:brew_crew/screens/announcements/announcements.dart';
import 'package:brew_crew/models/user.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';


class UserInformation extends StatefulWidget {
  @override
  _UserInformationState createState() => _UserInformationState();
}

class _UserInformationState extends State<UserInformation> {
  // Stream<QuerySnapshot> _usersStream =  FirebaseFirestore.instance.collection('createdClasses').snapshots();



  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery
        .of(context)
        .size;


    final user = Provider.of<UserClass>(context);

    String uid = user.uid;


    CollectionReference users = FirebaseFirestore.instance.collection(
        'joinedClasses').doc(uid).collection('_joinedClasses');


    return FutureBuilder<QuerySnapshot>(
      future: users.get(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasData) {
          print("Has data");
          print(snapshot.data);

          print(snapshot.data.docs.length);
        }
        else
          print("no data");
        // print(snapshot.hasError);
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Text("Loading");
        }


        return ListView(
          children: snapshot.data.docs.map((DocumentSnapshot document) {
            Map<String, dynamic> data = document.data() as Map<String, dynamic>;

            // _Class c = _Class(data['name'], data['description'], data['owner'], data['id']);
            return GestureDetector(


              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Announcements(
                        name: data['name'],
                        description: data['description'],
                        owner: data['owner'],
                        id: data['id'])));
              },


              child: Container(
                child: Row(
                  children: <Widget>[
                    Container(
                      height: 100,
                      decoration: BoxDecoration(
                      ),
                      child: Image.asset('assets/images/class.jpg'),
                    ),
                    SizedBox(
                      width: 40.0,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          data['name'],
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 30.0,
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Text(
                          data['description'],
                          style: TextStyle(
                            fontSize: 15.0,
                          ),
                        )
                      ],
                    ),
                  ],
                ),
                width: 10,
                margin:
                EdgeInsets.only(left: 10.0, top: 10.0, right: 10.0, bottom: 10.0),
                decoration: BoxDecoration(
                  color: Color(0xFFF1E6FF),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(5),
                      topRight: Radius.circular(5),
                      bottomLeft: Radius.circular(5),
                      bottomRight: Radius.circular(5)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 1,
                      blurRadius: 2,
                      offset: Offset(0, 1), // changes position of shadow
                    ),
                  ],
                ),
              ),
            );
          }).toList(),
        );
      },
    );
  }
}
