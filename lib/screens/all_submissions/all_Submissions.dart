import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class AllSubmissions extends StatefulWidget {
  final String class_id;
  final String assign_id;

  const AllSubmissions({Key key, this.class_id, this.assign_id})
      : super(key: key);

  @override
  _AllSubmissionsState createState() => _AllSubmissionsState();
}

class _AllSubmissionsState extends State<AllSubmissions> {
  Widget build(BuildContext context) {
    CollectionReference allSubmissions = FirebaseFirestore.instance
        .collection('submissions')
        .doc(widget.class_id)
        .collection('_assignments')
        .doc(widget.assign_id)
        .collection("_submissions");

    return Scaffold(
      appBar: AppBar(
        title: Text("All Submissions"),
        backgroundColor: Color(0xFF6F35A5),
      ),
      body: FutureBuilder<QuerySnapshot>(
        future: allSubmissions.get(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            print("Has data");
            print(snapshot.data);

            print(snapshot.data.docs.length);

            //	print(snapshot.data.docs[0]);

          } else
            print("no assignment");
          // print(snapshot.hasError);
          if (snapshot.hasError) {
            return Text('Something went wrong');
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text("Loading");
          }

          if (snapshot.data.docs.length == 0) {
            return Text("No submissions yet");
          }

          return ListView(
            children: snapshot.data.docs.map((DocumentSnapshot document) {
              Map<String, dynamic> data =
                  document.data() as Map<String, dynamic>;

              // _Class c = _Class(data['name'], data['description'], data['owner'], data['id']);
              return GestureDetector(
                child: Container(
                  child: Row(
                    children: <Widget>[
											Container(
												height: 50,
												decoration: BoxDecoration(),
												child: Icon(Icons.all_inbox,),
											),
											SizedBox(
												width: 40.0,
											),
											Column(
												crossAxisAlignment: CrossAxisAlignment.start,
												children: [
													if (data['urlDownload'] != null) ...[
														Text(data['student_name']),
														//Text(data['subm_id']),
														GestureDetector(
															onTap: () async {
																var url = data['urlDownload'];
																print(url);

																if (await canLaunch(url)) {
																	await launch(url);
																} else
																	print("could not launch");
															},
															child: Text("Download file"),
														),
													] else ...[
														Text(data['assign_id']),
														Text(data['subm_id']),
														//  Text(data['urlDownload']),
													]
												],
											),
                    ],
                  ),
									width: 10,
									margin: EdgeInsets.only(
											left: 10.0, top: 10.0, right: 10.0, bottom: 10.0),
									decoration: BoxDecoration(
										color: Color(0xFFF1E6FF),
										borderRadius: BorderRadius.only(
												topLeft: Radius.circular(5),
												topRight: Radius.circular(5),
												bottomLeft: Radius.circular(5),
												bottomRight: Radius.circular(5)),
										boxShadow: [
											BoxShadow(
												color: Colors.grey.withOpacity(0.5),
												spreadRadius: 1,
												blurRadius: 2,
												offset: Offset(0, 1), // changes position of shadow
											),
										],
									),
                ),
              );
            }).toList(),
          );
        },
      ),
    );
  }
}
