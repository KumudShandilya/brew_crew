import 'dart:io';

import 'package:brew_crew/models/user.dart';
import 'package:brew_crew/services/database.dart';
import 'package:brew_crew/shared/constants.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

class CreateAssignment extends StatefulWidget {
  final String class_id;

  const CreateAssignment({Key key, this.class_id}) : super(key: key);

  @override
  _CreateAssignmentState createState() => _CreateAssignmentState();
}

class _CreateAssignmentState extends State<CreateAssignment> {
  final _formKey = GlobalKey<FormState>();
  String assign_name = '';
  String assign_description = '';
  File file;
  UploadTask task;
  String urlDownload;

  Future selectFile() async {
    try {
      final result = await FilePicker.platform.pickFiles(allowMultiple: false);

      if (result == null) return;

      final path = result.files.single.path;

      setState(() => file = File(path));
    } catch (e) {
      print(e);
      return;
    }
  }

  Future uploadFile() async {
    if (file == null) return;

    var uuid = Uuid();

    final fileName = uuid.v4();

    final destination = 'assignments/$fileName';

    task = FirebaseApi.uploadFile(destination, file);

    if (task == null) return;

    final snapshot = await task.whenComplete(() {});

    //setState(() => urlDownload = await snapshot.ref.getDownloadURL(););

    final url = await snapshot.ref.getDownloadURL();

    setState(() => urlDownload = url);
  }

  Widget build(BuildContext context) {
    print(widget.class_id);
    Size size = MediaQuery.of(context).size;
    var uuid = Uuid();

    final user = Provider.of<UserClass>(context);

    String uid = user.uid;

    return Scaffold(
      appBar: AppBar(
        title: Text('Virtual Kaksha'),
        backgroundColor: Color(0xFF6F35A5),
      ),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
							Text("Create Assignment",
									style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
							SizedBox(height: size.height * 0.03),
							Image.asset(
								"assets/images/assign.png",
								height: size.height * 0.35,
							),
              SizedBox(height: 20.0),
              TextFormField(
                decoration:
                    textInputDecoration.copyWith(hintText: 'Class Name'),
                validator: (val) => val.isEmpty ? 'Enter announcement' : null,
                onChanged: (val) {
                  setState(() => assign_name = val);
                },
              ),
              SizedBox(height: 20.0),
              TextFormField(
                decoration:
                    textInputDecoration.copyWith(hintText: 'Class Description'),
                validator: (val) => val.isEmpty ? 'Enter description' : null,
                onChanged: (val) {
                  setState(() => assign_description = val);
                },
              ),
              SizedBox(height: 20.0),
              MaterialButton(
                color: Color(0xFF6F35A5),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)
                ),
                child: Text(
                  'Select File',
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () async {
                  await selectFile();
                },
              ),
              MaterialButton(
                color: Color(0xFF6F35A5),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)
                ),
                child: Text(
                  'Upload File',
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () async {
                  await uploadFile();
                },
              ),
              MaterialButton(
                  color: Color(0xFF6F35A5),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)
                  ),
                  child: Text(
                    'Create',
                    style: TextStyle(color: Colors.white),
                  ),
                  onPressed: () async {
                    if (_formKey.currentState.validate()) {
                      print(uid);
                      await DatabaseService(uid: uid).createAssignment(
                          widget.class_id,
                          assign_name,
                          assign_description,
                          uuid.v4(),
                          urlDownload);
                      setState(() => assign_description = "");
                      setState(() => assign_name = "");
                      setState(() => urlDownload = "");
                    }
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
