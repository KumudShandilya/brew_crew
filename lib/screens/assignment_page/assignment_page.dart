import 'dart:io';

import 'package:brew_crew/models/user.dart';
import 'package:brew_crew/screens/all_submissions/all_Submissions.dart';
import 'package:brew_crew/services/database.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:uuid/uuid.dart';

class AssignmentPage extends StatefulWidget {
  final String class_id;
  final String assign_id;
  final String name;
  final String description;
  final String urlDown;
  final String owner;

  const AssignmentPage(
      {Key key,
      this.class_id,
      this.assign_id,
      this.name,
      this.description,
      this.urlDown,
      this.owner})
      : super(key: key);

  _AssignmentPageState createState() => _AssignmentPageState();
}

class _AssignmentPageState extends State<AssignmentPage> {
  File file;
  UploadTask task;
  String urlDownload;

  onGoBack(dynamic value) {
    setState(() {});
  }

  void navigateToAllSubmissions() {
    Route route = MaterialPageRoute(
        builder: (context) => AllSubmissions(
            class_id: widget.class_id, assign_id: widget.assign_id));
    Navigator.push(this.context, route).then(onGoBack);
  }

  Future selectFile() async {
    try {
      final result = await FilePicker.platform.pickFiles(allowMultiple: false);

      if (result == null) return;

      final path = result.files.single.path;

      setState(() => file = File(path));
    } catch (e) {
      print(e);
      return;
    }
  }

  Future uploadFile() async {
    if (file == null) return;

    var uuid = Uuid();

    final fileName = uuid.v4();

    final destination = 'submissions/$fileName';

    task = FirebaseApi.uploadFile(destination, file);

    if (task == null) return;

    final snapshot = await task.whenComplete(() {});

    //setState(() => urlDownload = await snapshot.ref.getDownloadURL(););

    final url = await snapshot.ref.getDownloadURL();

    setState(() => urlDownload = url);
  }

  Widget build(BuildContext context) {
    //print(widget.class_id);

    var uuid = Uuid();

    final user = Provider.of<UserClass>(context);

    String uid = user.uid;
    String user_name = user.user_name;

    bool is_owner = false;

    if (widget.owner == uid) is_owner = true;

    return Scaffold(
      appBar: AppBar(
        title: Text('Your assignment'),
        backgroundColor: Color(0xFF6F35A5),
        actions: <Widget>[
          /*Visibility(
              visible: is_owner,
              child: FlatButton.icon(
                icon: Icon(Icons.person),
                label: Text('See all submissions'),
                onPressed: () {
                  navigateToAllSubmissions();
                },
              )),*/
        ],
      ),
      floatingActionButton: Visibility(
          visible: is_owner,
          child: FloatingActionButton(
            onPressed: () {
              navigateToAllSubmissions();
            },
            child: Icon(Icons.all_inbox),
            backgroundColor: Color(0xFF6F35A5),
          )),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 50.0),
        child: Form(
          child: Column(
            children: <Widget>[
              SizedBox(height: 20.0),
              Text(widget.name),
              SizedBox(height: 20.0),
              Text(widget.description),
              SizedBox(height: 20.0),
              if (widget.urlDown != null) ...[
                GestureDetector(
                  onTap: () async {
                    var url = widget.urlDown;
                    print(url);

                    if (await canLaunch(url)) {
                      await launch(url);
                    } else
                      print("could not launch");
                  },
                  child: Text("Download Assignment"),
                ),
              ],
              Visibility(
                  visible: !is_owner,
                  child: MaterialButton(
                    color: Color(0xFF6F35A5),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)
                    ),
                    child: Text(
                      'Select File',
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () async {
                      await selectFile();
                    },
                  )),
              Visibility(
                  visible: !is_owner,
                  child: MaterialButton(
                    color: Color(0xFF6F35A5),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)
                    ),
                    child: Text(
                      'Upload File',
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () async {
                      await uploadFile();
                    },
                  )),
              Visibility(
                  visible: !is_owner,
                  child: MaterialButton(
                      color: Color(0xFF6F35A5),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)
                      ),
                      child: Text(
                        'Create',
                        style: TextStyle(color: Colors.white),
                      ),
                      onPressed: () async {
                        //print(uid);
                        await DatabaseService(uid: uid).submitAssignment(
                            widget.class_id,
                            widget.assign_id,
                            uuid.v4(),
                            urlDownload,
                            user_name);

                        setState(() => urlDownload = "");
                      })),
            ],
          ),
        ),
      ),
    );
  }
}
