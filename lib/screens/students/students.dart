import 'package:brew_crew/models/user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Students extends StatefulWidget {
  @override
  final String class_id;

  const Students({Key key, this.class_id}) : super(key: key);

  _StudentsState createState() => _StudentsState();
}

class _StudentsState extends State<Students> {
  // Stream<QuerySnapshot> _usersStream =  FirebaseFirestore.instance.collection('createdClasses').snapshots();

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserClass>(context);

    String uid = user.uid;

    CollectionReference students = FirebaseFirestore.instance
        .collection('students')
        .doc(widget.class_id)
        .collection('_students');

    return Scaffold(
      appBar: AppBar(
        title: Text("Students"),
          backgroundColor: Color(0xFF6F35A5),
      ),
      body: FutureBuilder<QuerySnapshot>(
        future: students.get(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            print("Has data");
            print(snapshot.data);

            print(snapshot.data.docs.length);
          } else
            print("no data");
          // print(snapshot.hasError);

          if (snapshot.hasError) {
            return Text('Something went wrong');
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text("Loading");
          }

          if (snapshot.data.docs.length == 0) return Text("No students",
            textAlign: TextAlign.center,);



          return ListView(
            children: snapshot.data.docs.map((DocumentSnapshot document) {
              Map<String, dynamic> data =
                  document.data() as Map<String, dynamic>;

              // _Class c = _Class(data['name'], data['description'], data['owner'], data['id']);
              return GestureDetector(
                child: Container(
                  child: Row(
                    children: <Widget>[
                      Container(
                        height: 100,
                        decoration: BoxDecoration(
                        ),
                        child: Icon(Icons.person),
                      ),
                      SizedBox(
                        width: 40.0,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            data['student_id'],
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 30.0,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              );
            }).toList(),
          );
        },
      ),
    );
  }
}
