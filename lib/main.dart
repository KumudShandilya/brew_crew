
import 'package:brew_crew/screens/authenticate/Welcome/welcome.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:brew_crew/screens/wrapper.dart';
import 'package:brew_crew/services/auth.dart';
import 'package:provider/provider.dart';
import 'package:brew_crew/models/user.dart';

import 'package:brew_crew/screens/loading.dart';
import 'package:brew_crew/screens/create_class/create_class.dart';
import 'package:brew_crew/screens/join_class/join_class.dart';


void main() {
  runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      // Initialize FlutterFire
      future: Firebase.initializeApp(),
      builder: (context, snapshot) {
        // Check for errors
        if (snapshot.hasError) {
          return MaterialApp(
          	home: Text('Wrong'),
          );
        }

        // Once complete, show your application
        if (snapshot.connectionState == ConnectionState.done) {
         return StreamProvider<UserClass>.value(
      value: AuthService().user,
      child: MaterialApp(
        home: Welcome(),
      ),
    );
        }

        // Otherwise, show something whilst waiting for initialization to complete
        return MaterialApp(
        	home: Text('Loading'),
        );
      },
    );
  }
}
