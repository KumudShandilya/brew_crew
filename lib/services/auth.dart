//import 'package:firebase_core/firebase_core.dart';
import 'package:brew_crew/models/user.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:brew_crew/services/database.dart';


class AuthService {

 // final FirebaseAuth _auth = FirebaseAuth.instance;
	FirebaseAuth _auth = FirebaseAuth.instance;
  // sign in anon
  
  
 // UserCredential userCredential = await FirebaseAuth.instance.signInAnonymously();
 
 
 
 
 UserClass _userFromFirebaseUser(User user) {
    return user != null ? UserClass(uid: user.uid, user_name: user.email) : null;
  }
  
  
    Stream<UserClass> get user {
    return _auth.authStateChanges().map((User user) => _userFromFirebaseUser(user));
      //
      
  }

  
  Future signInAnon() async {
    try {
     // AuthResult result = await _auth.signInAnonymously();
     // FirebaseUser user = result.user;
     	UserCredential userCredential = await FirebaseAuth.instance.signInAnonymously();
     	User user = userCredential.user;
     	//print(userCredential);
      return _userFromFirebaseUser(user);
    } catch (e) {
      print(e.toString());
      return null;
    }
  }

  // sign in with email and password
  
  
  Future signInWithEmailAndPassword(String email, String password) async {
    try {
      UserCredential userCredential = await _auth.signInWithEmailAndPassword(email: email, password: password);
      User user = userCredential.user;
  
      return user;
    } catch (error) {
      print(error.toString());
      return null;
    } 
  }


  // register with email and password
  
   Future registerWithEmailAndPassword(String email, String password) async {
    try {
      UserCredential userCredential = await _auth.createUserWithEmailAndPassword(email: email, password: password);
      User user = userCredential.user;
      
  
      
     //  await DatabaseService(uid: user.uid, id: uuid.v4()).updateUserData('Class1', 'Test1');
      return _userFromFirebaseUser(user);
    } catch (error) {
      print(error.toString());
      return null;
    } 
  }
  

  // sign out
  
   Future signOut() async {
    try {
      return await _auth.signOut();
    } catch (error) {
      print(error.toString());
      return null;
    }
  }

}
