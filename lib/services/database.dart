import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'dart:io';



class DatabaseService {

  final String uid;
  DatabaseService({ this.uid});
  
  
  // collection reference
  final CollectionReference createdClasses = FirebaseFirestore.instance.collection('createdClasses');
  final CollectionReference joinedClasses = FirebaseFirestore.instance.collection('joinedClasses');
  final CollectionReference announcements = FirebaseFirestore.instance.collection('announcements');
  final CollectionReference assignments = FirebaseFirestore.instance.collection('assignments');
  final CollectionReference submissions = FirebaseFirestore.instance.collection('submissions');
  final CollectionReference allClasses = FirebaseFirestore.instance.collection('allClasses');
  final CollectionReference students = FirebaseFirestore.instance.collection('students');

  Future<void> createNewClass(String name, String description, String id) async {
    await createdClasses.doc(uid).collection("_createdClasses").doc(id).set({
     	 'owner': uid,
        'name': name,
        'description': description,
        'id': id,
    });
    
    
    await allClasses.doc(id).set({
  	
  	    'owner': uid,
        'name': name,
        'description': description,
        'id': id,	
  	  
  	
  	});
  	
  	
  	
  return await joinedClasses.doc(uid).collection("_joinedClasses").doc(id).set({
  	
  		'owner': uid,
  		'name': name,
  		'description': description,
  		'id': id,
  	
  	});
    
    
  }
  
  Future<void> createAnnouncement(String class_id, String announce_name, String announce_description, String id, String urlDownload) async {
  
  	return  await announcements.doc(class_id).collection("_announcements").doc(id).set({
  	
  		'announce_name': announce_name,
  		'announce_description': announce_description,
  		'id': id,
  		'class_id': class_id,
  		'urlDownload': urlDownload
  	
  	});
  	
  	
  	
  
  
  }
  
  
  Future<void> submitAssignment(String class_id, String assign_id, String subm_id, String urlDownload, String user_name) async
  {
  
  	return await submissions.doc(class_id).collection("_assignments").doc(assign_id).collection("_submissions").doc(subm_id).set({
  	
  	
  		'class_id': class_id,
  		'assign_id': assign_id,
  		'subm_id': subm_id,
  		'urlDownload': urlDownload,
  		'student_id': uid,
  		'student_name': user_name,
  	
  	
  	
  	
  	});
  
  }
  
  
  Future<void> createAssignment(String class_id, String assign_name, String assign_description, String id, String urlDownload) async {
  
  	return await assignments.doc(class_id).collection("_assignments").doc(id).set({
  	
  		'assign_name': assign_name,
  		'assign_description': assign_description,
  		'id': id,
  		'class_id': class_id,
  		'urlDownload': urlDownload
  	
  	});
  
  
  }
  
  
  
  
  
  
  
  Future<void> joinClassUsingCode(String classCode) async {
  
  	print("called");
  	var temp = await allClasses.doc(classCode).get();
  	
  	if(temp.exists) {
  	
  	var data = temp.data();
  	print(data['name']);
  	
  	 if (data['owner'] != uid) 
  	
  	{ await joinedClasses.doc(uid).collection("_joinedClasses").doc(classCode).set({
  	
  		'owner': data['owner'],
  		'name': data['name'],
  		'description': data['description'],
  		'id': classCode,
  	
  	});
  	
  	
  	return await students.doc(classCode).collection('_students').doc(uid).set({
  	
  		'student_id': uid,
  	
  	});
  	
  	
  	
  	
  	}
  	else print('Class is created by you');
  	}
  	else print('Class not found');
  }
    
    
    
   Future<void> printdata() {
   
   print(uid);
   
   //QuerySnapshot temp =  createdClasses.doc(uid).collection("_createdClasses").snapshots().first;
   
   //print(temp.data);
   
  
  /* for(int i=0; i<temp.length; i++)
   {
      print(temp[i]['name']);
   }*/
   
   
   
   
   }
   
   
 
   
   
   
   
   
    /*Stream<QuerySnapshot> get classesThatYouCreated {
    return createdClasses.doc(uid).collection("_createdClasses").snapshots();
  }*/

}



class FirebaseApi {


	static UploadTask uploadFile(String destination, File file)
	{
		try {
   	
	   		final ref = FirebaseStorage.instance.ref(destination);
	   		
	   		//print(file);

			return ref.putFile(file);   
		   	
		   	
		   } catch(e) {
		   	print(e);
		   
		   	return null;
		   
		   }
	
	}


}
