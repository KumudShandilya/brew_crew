class _Class {

  final String name;
  final String description;
  final String owner;
  final String id;

  _Class({ this.name, this.description, this.owner, this.id });

}

